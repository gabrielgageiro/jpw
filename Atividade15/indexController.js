class Gerador {
    constructor (){
    }
    gerarHTML(){
        return document.createElement('div');
    }
}

class GeradorLista extends Gerador {
    constructor(){
        super();
        this.lista = [];
    }

    addLista(value){
        this.lista.push(value);
    }

    getLista(){
        return this.lista;
    }
}

var geraLista = new GeradorLista();

geraLista.addLista("Avaí");
geraLista.addLista("Chapecoense");
geraLista.addLista("Criciúma");
geraLista.addLista("Figueirense");
geraLista.addLista("Joinville");

var div = geraLista.gerarHTML();
for (const item of geraLista.getLista()) {
    var element = document.createElement('p');
    element.innerHTML = item;
    div.appendChild(element);
    document.body.appendChild(div);
}