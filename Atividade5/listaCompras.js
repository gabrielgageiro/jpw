var list = [];

function atualizaLista(){
    var valor = parseFloat(document.getElementById("valor").value);
    if(valor <= 0 || isNaN(valor)) return;
    list.push({item : document.getElementById("item").value, valor : valor});
    document.getElementById("total").value = contaLista();
    document.getElementById("lista").innerHTML = mostraLista();
}

function contaLista(){
    var conta = 0;
    list.forEach(element => {
        conta += element.valor;
    });
    document.getElementById("total").style.borderColor = valor > 50 ? "red" : "white";
    return conta;
}

function mostraLista(){
    var itens = "";
    list.forEach(element => {
        itens += "Item: " + element.item +" - Preço: "+ element.valor +"<br>";
    })
    return itens;
}