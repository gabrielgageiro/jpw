import React from 'react'
import Axios from 'axios'
import Computador from './Computador'

export default class PaginaComputador extends React.Component {
    constructor(props) {
        super(props)
        this.BASE_URL = "http://localhost:3001/computador"

        this.state = { computadores: [] }

    }

    componentDidMount = () => {
        this.handleGet()
    }

    handleGet = () => {
        const url = 'http://localhost:3001/computador'
        var request = Axios.get(url)

        request.then((response) => {
            this.setState({
                computadores: response.data
            })
        })
    }

    handleInsert = (data) => {
        Axios.post(this.BASE_URL, data).then((resp) => {
            this.loadComp()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleDelete = (value) => {
        Axios.delete("http://localhost:3001/computador/" + value).then(() => {
            this.loadComp()
        })
    }

    loadComp = () => {
        Axios.get(this.BASE_URL).then((response) => {
            this.setState({ computadores: response.data })
        }).catch((error) => {
            console.error(error)
        })
    }

    render() {
        var list = this.state.computadores.map((item, value) => {
            return (<div>
                            <span>Hostname: {item.hostname};</span>
                            <span>Processador: {item.processador};</span>
                            <span>Memória: {item.memoria};</span>
                            <span>Armazenamento: {item.armazenamento};</span>
                            <span>Estado: {item.estado};</span>
                            <span><button onClick={() => { this.handleDelete(value) }}>Excluir</button></span>

            </div>)
        })
        return (<div>
            <Computador handleAction={this.handleInsert}></Computador>
            {list}
        </div>)
    }
}