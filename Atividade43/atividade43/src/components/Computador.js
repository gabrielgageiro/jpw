import React from 'react'

export default class Modo extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            hostname: "",
            processador: "",
            memoria: "",
            armazenamento: "",
            estado: ""
        }
    }

    handleChange = (event) => {
        this.setState({
            [event.target.id]: event.target.value
            //    [inputName]: inputValue
        })
    }

    handleAction = (event) => {
        event.preventDefault()
        var data = {
            hostname: this.state.hostname,
            processador: this.state.processador,
            memoria: this.state.memoria,
            armazenamento: this.state.armazenamento,
            estado: this.state.estado
        }
        //var data = { "name": this.state.name, "password": this.state.password }
        this.props.handleAction(data)
    }

    render() {
        return (<form onSubmit={this.handleAction}>
            Hostname: <input type="text" id="hostname" value={this.state.hostname} onChange={this.handleChange}></input> <br />
            Processador: <input type="text" id="processador" value={this.state.processador} onChange={this.handleChange} /><br />
            Memória: <input type="text" id="memoria" value={this.state.memoria} onChange={this.handleChange} /><br />
            Armazenamento: <input type="text" id="armazenamento" value={this.state.armazenamento} onChange={this.handleChange} /><br />
            Estado: <input type="text" id="estado" value={this.state.estado} onChange={this.handleChange} /><br />
            <button type="submit">Novo</button>
        </form>)
    }
}