import React from 'react';
import logo from './logo.svg';
import './App.css';
import PaginaComputador from './components/PaginaComputador';

function App() {
  return (
    <div className="App">
      <PaginaComputador></PaginaComputador>  
    </div>
  );
}

export default App;
