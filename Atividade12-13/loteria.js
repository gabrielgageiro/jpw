var list = [];
var compradores = [];

function comprar(){
    var nums = geraSeq();
    var name = document.getElementById("nome").value;
    compradores.push({nome:name,num:nums});
    document.getElementById("compradores").innerHTML += "Comprador: " + name + " Números: " + nums.sort(function(a, b){return a-b}) + "<br>";
}

function gerarNumeros(){
    list = [];
    list = geraSeq();
    document.getElementById("lista").innerHTML = list.sort(function(a, b){return a-b});
    document.getElementById("resultado").innerHTML = "";
    compradores.forEach(compra => {
        var check = [];
        compra.num.forEach(el => {
            list.forEach(l => {
                if(el == l)
                check.push(el)
            })
        });
    document.getElementById("resultado").innerHTML += "Comprador: " + compra.nome + " - Acertos: (" + check.length + "/6) " + check + "<br>";
    if(check.length == 6)
        alert("Ganhador: " + compra.nome);
        
});
}

function geraSeq(){
    var nums = [];
    while(nums.length != 6){
        var num = Math.floor(Math.random() * 50 + 1);
        if(nums.indexOf(num) < 0)
            nums.push(num);
    }
    return nums;
}