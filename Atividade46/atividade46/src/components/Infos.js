import React from 'react'
import Entrada from './Entrada'

export default class Infos extends React.Component {
    constructor(props) {
        super(props)
        this.state = { inv: 0, meses: 0, total: 0 }
    }
    handleMesChange = (unidade) => {
        this.setState({
            meses: unidade,
            total: unidade * this.state.inv
        })
    }

    handleInvChange = (unidade) => {
        this.setState({
            inv: unidade,
            total: this.state.meses * unidade
        })
    }

    handleTotalChange = (unidade) => {
        if (this.state.meses == 0)
            this.state.meses = 1
        this.setState({
            total: unidade,
            inv: unidade / this.state.meses
        })
    }

    render() {
        return <div>
            Investimento: <Entrada valor={this.state.inv} onUnitChange={this.handleInvChange} />
            Meses: <Entrada valor={this.state.meses} onUnitChange={this.handleMesChange} />
            Total: <Entrada valor={this.state.total} onUnitChange={this.handleTotalChange} />
        </div>
    }
}