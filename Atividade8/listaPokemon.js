var list = [];
var indexList = -1;

function atualizaLista(){
    if(indexList < 0){
    list.push({
        nome : document.getElementById("nome").value,
        estilo : document.getElementById("estilo").value,
        imagem : document.getElementById("imagem").value,
    });
}
else{
    list[indexList].nome = document.getElementById("nome").value
    list[indexList].estilo = document.getElementById("estilo").value
    list[indexList].imagem = document.getElementById("imagem").value
}
    indexList = -1;
    document.getElementById("lista").innerHTML = mostraLista();
}

function contaLista(){
    var conta = 0;
    list.forEach(element => {
        conta += element.valor;
    });
    return conta;
}

function alterar(index){
    document.getElementById("nome").value = list[index].nome;
    document.getElementById("estilo").value = list[index].estilo;
    document.getElementById("imagem").value = list[index].imagem;
    indexList = index;
}

function deletar(index){
    //var titulo = document.getElementById("titulo").value;
    //for(var i = 0; i < list.length; i++) {
        //if(list[i].nome == titulo){
            list.splice(index,1);
            document.getElementById("lista").innerHTML = mostraLista();
            return;
       //}
    //}   
}

function mostraLista(){
    var itens = "";
    var contador = 0;
    list.forEach(element => {
        itens += "<div><b>Nome:</b> "+ element.nome +" <b>Estilo:</b>"+ element.estilo +"<img src='"+ element.imagem +"'><button onclick='alterar("+ contador +")' type='button'>Alterar</button><button onclick='deletar("+ contador +")' type='button'>Deletar</button></div>";
        contador++;
    })
    return itens;
}