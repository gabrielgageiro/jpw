var express = require('express');
var app = express();
const fs = require('fs')
app.use(express.json())

app.get('/gerador', function(req, res){
    var file = fs.readFile("nomes.json", function(error, content){
        var objeto = JSON.parse(content);
        res.send(objeto.nome[Math.floor(Math.random() * (objeto.nome.length))] + ' ' + objeto.sobrenome[Math.floor(Math.random() * (objeto.sobrenome.length))] + ' é um futebolista brasileiro de ' + [Math.floor(Math.random() * (40 - 17) + 17)] + ' anos que atua como '+objeto.posicao[Math.floor(Math.random() * (objeto.posicao.length))]+'. Atualmente defende: ' + objeto.clube[Math.floor(Math.random() * (objeto.clube.length))] + '.')    
    })
})

app.get('/jogador', function(req, res){
    var file = fs.readFile("nomes.json", function(error, content){
        var objeto = JSON.parse(content);
        var idade = [Math.floor(Math.random() * (40 - 17) + 17)]
        res.send(objeto.nome[Math.floor(Math.random() * (objeto.nome.length))] + ' ' + objeto.sobrenome[Math.floor(Math.random() * (objeto.sobrenome.length))] + ' é um futebolista brasileiro de ' + idade + ' (' + filtraIdade(idade) + ') anos que atua como '+objeto.posicao[Math.floor(Math.random() * (objeto.posicao.length))]+'. Atualmente defende: ' + objeto.clube[Math.floor(Math.random() * (objeto.clube.length))] + '.')    
    })
})

app.post('/jogador', function(req, res){
 
    var lista = null;
    var file = fs.readFileSync('nomes.json', 'utf8');
    lista = JSON.parse(file);

     var obj = JSON.stringify(req.body);    
     var novos = JSON.parse(obj)

    if(novos['nome']){
        lista['nome'].push(novos.nome)
    }

    if(novos.clube){
        lista.clube.push(novos.clube)
    }   
    
    fs.writeFileSync('nomes.json', JSON.stringify(lista), {encoding: 'utf-8', flag: 'w'});
    res.end() 
})

app.listen(3000,function(){
    console.log("Aplicação rodando na porta 3000")
})
// 17 a 22 novato
// 23-28 profissional
// 29-34 veterano
// 35-40 master
function filtraIdade(idade){
    if(idade < 23) return 'novato'
    else if(idade < 29)return 'profissional'
    else if(idade < 35)return 'veterano'
    else return 'master'

}