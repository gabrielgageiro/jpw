import React from 'react'

export default class Velocidade extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            vel: 1
        }
    }


    muda = () => {
        this.setState({ vel: this.state.vel === 3 ? 1 : this.state.vel + 1 })
    }

    render() {
        return (<div>Velocidade:
            <button onClick={this.muda}> {this.state.vel} </button></div>)
    }
}