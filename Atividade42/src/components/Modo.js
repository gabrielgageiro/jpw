import React from 'react'

export default class Modo extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            modo: 0,
            modos: ['Aquecer', 'Refrigerar', 'Ventilar']
        }
    }


    muda = () => {
        this.setState({ modo: this.state.modo === 2 ? 0 : this.state.modo + 1 })
    }

    render() {
        return (<div>Modo de funcionamento:
            <button onClick={this.muda}> {this.state.modos[this.state.modo]} </button></div>)
    }
}