import React from 'react';
import Temperatura from './components/Temperatura'
import Modo from './components/Modo'
import Velocidade from './components/Velocidade'
import './App.css';

function App() {
  return (
    <div className="App">
      <Temperatura></Temperatura>    
      <Modo></Modo>    
      <Velocidade></Velocidade>    
    </div>
  );
}

export default App;
