    function check(e) {
        var nome = document.getElementById("nome").value;
        var raca = document.getElementById("raca").value;
        var cidade = document.getElementById("cidade").value;
        if(nome.length < 3){
            alert("Nome deve conter ao menos três caracteres");
            e.returnValue = false;
        }
        else if(!validaNome(nome.toUpperCase())){
            alert("Nome iniciando com termos que não atendem aos requisitos (Admin, GM ou Moderador)")
            e.returnValue = false;            
        }
        // else if(nome.substring(0,2) == 'GM'){
        //     alert("Nome não pode iniciar com 'GM'");
        //     e.returnValue = false;
        // }
        else if(raca == "elfo" && cidade == "venore"){
            alert("Elfos não podem residir em Venore");
            e.returnValue = false;
        }
        else if(raca == "anao" && cidade == "carlin"){
            alert("Anões não podem residir em Venore");            
            e.returnValue = false;
        }
    }

    function validaNome(nome){
        if(nome.length > 8){
            if(nome.substring(0,9) == 'MODERADOR')
                return false;
        }
        if(nome.length > 4){       
            if(nome.substring(0,5) == 'ADMIN')
                return false;
        }        
        return nome.substring(0,2) != 'GM';
    }